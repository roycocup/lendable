<?php
/**
 * Unit tests for Round up functions
 */
namespace Lendable\Interview\Interpolation\Tests;

use  Lendable\Interview\Interpolation\Service\ServiceHelper;

class ServiceHelperUnitTest extends Basecase
{
    public function test_round_up_will_always_be_multiple()
    {
        $actual = ServiceHelper::roundUpToAny(1);
        $this->assertEquals(5, $actual);

        $actual = ServiceHelper::roundUpToAny(5);
        $this->assertEquals(5, $actual);

        $actual = ServiceHelper::roundUpToAny(6);
        $this->assertEquals(10, $actual);

        $actual = ServiceHelper::roundUpToAny(-109);
        $this->assertEquals(-105, $actual);

        $actual = ServiceHelper::roundUpToAny(-102);
        $this->assertEquals(-100, $actual);

        $actual = ServiceHelper::roundUpToAny(2, 2);
        $this->assertEquals(2, $actual);

        $actual = ServiceHelper::roundUpToAny(2, 3);
        $this->assertEquals(3, $actual);

        $actual = ServiceHelper::roundUpToAny(12, 7);
        $this->assertEquals(14, $actual);
        
    }
}