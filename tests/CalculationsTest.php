<?php
/**
 * Calculation tests
 */
namespace Lendable\Interview\Interpolation\Tests;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculator;

class CalculationsTest extends Basecase
{
    public function test_can_get_correct_example_fee_which_is_not_on_tables()
    {
        $calculator = new FeeCalculator();
        $application = new LoanApplication(24, 2750);
        $fee = $calculator->calculate($application);

        $this->assertEquals(115.0, $fee);
    }

    public function TwelveMonthsTermProvider()
    {
        return [
            [1000, 50],
            [2000, 90],
            [3000, 90],
            [4000, 115],
            [5000, 100],
            [6000, 120],
            [7000, 140],
            [8000, 160],
            [9000, 180],
            [10000, 200],
            [11000, 220],
            [12000, 240],
            [13000, 260],
            [14000, 280],
            [15000, 300],
            [16000, 320],
            [17000, 340],
            [18000, 360],
            [19000, 380],
            [20000, 400],
        ];
    }

    public function TwentyFourMonthsTermProvider()
    {
        return [
            [1000, 70],
            [2000, 100],
            [3000, 120],
            [4000, 160],
            [5000, 200],
            [6000, 240],
            [7000, 280],
            [8000, 320],
            [9000, 360],
            [10000, 400],
            [11000, 440],
            [12000, 480],
            [13000, 520],
            [14000, 560],
            [15000, 600],
            [16000, 640],
            [17000, 680],
            [18000, 720],
            [19000, 760],
            [20000, 800],
        ];
    }

    /**
     * @dataProvider TwelveMonthsTermProvider
     */
    public function test_12_month_term_structure_produces_correct_results($amount, $expectedFee)
    {
        $calculator = new FeeCalculator();
        $application = new LoanApplication(12, $amount);
        $actualFee = $calculator->calculate($application);

        $this->assertEquals($expectedFee, $actualFee);
    }

    /**
     * @dataProvider TwentyFourMonthsTermProvider
     */
    public function test_24_month_term_structure_produces_correct_results($amount, $expectedFee)
    {
        $calculator = new FeeCalculator();
        $application = new LoanApplication(24, $amount);
        $actualFee = $calculator->calculate($application);

        $this->assertEquals($expectedFee, $actualFee);
    }

    public function test_fee_is_multiple_of_5()
    {
        $calculator = new FeeCalculator();
        $application = new LoanApplication(24, 1923);
        $actualFee = $calculator->calculate($application);

        $this->assertEquals(0, $actualFee % 5);
    }
}