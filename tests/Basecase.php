<?php

namespace Lendable\Interview\Interpolation\Tests;

use PHPUnit\Framework\TestCase;

abstract class Basecase extends TestCase
{
    
    public function dd($o)
    {
        var_dump($o);
        die;
    }

}
