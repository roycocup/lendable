<?php

/**
 * Tests for the Application general working
 */

namespace Lendable\Interview\Interpolation\Tests;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculator;

class AppTest extends Basecase
{
    /**
     * @expectedException Lendable\Interview\Interpolation\Exceptions\ApplicationAmountException
     */
    public function test_min_loan_amounts()
    {
        // test minimum
        $a1 = new LoanApplication(12, 1);
        $a2 = new LoanApplication(24, 1);
    }

    public function test_min_and_max_loan_amounts_are_ok()
    {
        $aWithMinimum = new LoanApplication(12, 1000);
        $aWithMaximum = new LoanApplication(12, 20000);

        $this->assertInstanceOf(LoanApplication::class, $aWithMinimum);
        $this->assertInstanceOf(LoanApplication::class, $aWithMaximum);
    }

    public function test_min_and_max_loan_amounts_are_ok_for_24()
    {
        $aWithMinimum = new LoanApplication(24, 1000);
        $aWithMaximum = new LoanApplication(24, 20000);

        $this->assertInstanceOf(LoanApplication::class, $aWithMinimum);
        $this->assertInstanceOf(LoanApplication::class, $aWithMaximum);
    }

    /**
     * @expectedException Lendable\Interview\Interpolation\Exceptions\ApplicationAmountException
     */
    public function test_max_loan_amounts()
    {
        // test maximum
        $a3 = new LoanApplication(12, pow(10, 6)); // a million 
        $a4 = new LoanApplication(24, pow(10, 6)); // a million 
    }
}