# Task for Lendable

## How to run
1. Run composer by doing `composer install`
2. Run all tests by calling `vendor/bin/phpunit` 

## Things I would have done better
1. The `FeeCalculator` class is too complex breaking the `Simple Responsibility Principle` and possibly the `Open-closed Principle`. So I tried refactoring it to use 2 subclasses named `FeeTableCalculator` and `FeeInterpolationCalculator` where we could just instantiate whichever applied and call `calculate` on one of them as per interface. However, I found that moving the conditional of whether the fee was present on the table or not, is cohesive and too familiar with the whole process of returning the fee altogether, so at this point I left the code all on the same class and I would open a discussion on the code review on whether I should have carried that or not. 
2. I didn't really do much testing `after the fact` which accounts for the fact that I'm not testing for multiple uncaught types of errors that could potentially come up.


