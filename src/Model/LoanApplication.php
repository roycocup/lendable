<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Exceptions;
namespace Lendable\Interview\Interpolation\Model;

use Lendable\Interview\Interpolation\Exceptions\ApplicationAmountException;


/**
 * A cut down version of a loan application containing
 * only the required properties for this test.
 */
class LoanApplication
{
    /**
     * @var int
     */
    private $term;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    protected $minimumAmount = 1000;

    /**
     * @var float
     */
    protected $maximumAmount = 20000;



    /**
     * @param int $term
     * @param float $amount
     */
    public function __construct(int $term, float $amount)
    {
        $this->term = $term;
        
        $this->setAmount($amount);
    }

    /**
     * Gets the term for this loan application expressed
     * in number of months.
     *
     * @return int
     */
    public function getTerm(): int
    {
        return $this->term;
    }

    /**
     * Gets the amount requested for this loan application.
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * Sets the amount for a requestes loan application
     */
    public function setAmount(float $amount)
    {
        if ($amount < $this->minimumAmount)
        {
            throw new ApplicationAmountException("Application amount is too low. Amount should be greater or equal to ".$this->minimumAmount);
        }
        if ($amount > $this->maximumAmount)
        {
            throw new ApplicationAmountException("Application amount is too high. Amount should be smaller or equal to ".$this->maximumAmount);
        }
        $this->amount = $amount;
    }
}
