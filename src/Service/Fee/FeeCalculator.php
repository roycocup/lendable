<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee;

use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\ServiceHelper;

class FeeCalculator implements FeeCalculatorInterface
{

    /**
     * @var array $fees - Lookup tables with Loan Amount and corresponding fee value
     */
    protected $fees = [
        12 => [
            1000 => 50,
            2000 => 90,
            3000 => 90,
            4000 => 115,
            5000 => 100,
            6000 => 120,
            7000 => 140,
            8000 => 160,
            9000 => 180,
            10000 => 200,
            11000 => 220,
            12000 => 240,
            13000 => 260,
            14000 => 280,
            15000 => 300,
            16000 => 320,
            17000 => 340,
            18000 => 360,
            19000 => 380,
            20000 => 400,
        ],
        24 => [
            1000 => 70,
            2000 => 100,
            3000 => 120,
            4000 => 160,
            5000 => 200,
            6000 => 240,
            7000 => 280,
            8000 => 320,
            9000 => 360,
            10000 => 400,
            11000 => 440,
            12000 => 480,
            13000 => 520,
            14000 => 560,
            15000 => 600,
            16000 => 640,
            17000 => 680,
            18000 => 720,
            19000 => 760,
            20000 => 800,
        ],
    ];
    /**
     * Calculates the fee for a loan application.
     *
     * @param LoanApplication $application The loan application to
     * calculate for.
     *
     * @return float The calculated fee.
     */
    public function calculate(LoanApplication $application): float
    {
        $fee = $this->getFeeFromTable($application->getTerm(), $application->getAmount());

        if ($fee == null)
        {
            $calculatedFee = $this->getInterpolatedFee($application->getTerm(), $application->getAmount());
        } else {
            $calculatedFee = $fee;
        }

        return ServiceHelper::roundUpToAny($calculatedFee);
    }

    /**
     * Fetches the fee value for a specific loan amount on the specified table(s)
     * 
     * @param int $term - the loan term, eg, 12 or 24 months
     * @param float $amount - the loan amount 
     * 
     * @return int|null A fee for the term/amount. Null if not found.
     */
    public function getFeeFromTable(int $term, float $amount): ?int
    {
        return $this->fees[$term][$amount] ?? null;
    }

    /**
     * Calculates an interpolated fee between an upper and a lower bound on a given set
     *  
     * @param int $term - the loan term, eg, 12 or 24 months
     * @param float $amount - the loan amount 
     * 
     * @return int A fee for the term/amount
     */
    public function getInterpolatedFee(int $term, float $amount): int
    {
        // get interval points
        $interval = $this->getIntervalPoints($term, $amount);

        // interpolate value
        $interpolatedFee = $this->lerp(
            $interval['minAmount'], 
            $interval['minFee'], 
            $amount,
            $interval['maxAmount'], 
            $interval['maxFee']
        );

        // round interpolated value
        return (int) round($interpolatedFee);
    }

    /**
     * Returns the upper and lower bounds for a loan amount including the corresponding fee values
     * @param int $term - the loan term, eg, 12 or 24 months
     * @param float $amount - the loan amount 
     * 
     * @return array An array with the upper and lower amounts and corresponding fees 
     */
    public function getIntervalPoints(int $term, float $amount): array
    {
        $min = null;
        $max = null;
        $table = $this->fees[$term];
        
        reset($table);
        while($tableFee = current($table))
        {
            // $tableFee is the fee value on the table, 
            // but we would like to use the key as it represents the amount
            // which we want to compare against, so we can get that by flipping the array
            // and obtaining the array key as $tableAmount.
            $tableAmount = \array_flip($table)[$tableFee];

            if($amount < $tableAmount)
            {
                $previousFee = prev($table);

                $maxAmount = $tableAmount;
                // flip the table and the index becomes the fee (so we get the amount)
                $minAmount = \array_flip($table)[$previousFee]; 
                $maxFee = $tableFee;
                $minFee = $previousFee;

                break;
            }
            
            //advance the array pointer one step
            next($table);
        }
        
        return [
            'minFee' => $minFee, 
            'maxFee' => $maxFee,
            'minAmount' => $minAmount, 
            'maxAmount' => $maxAmount, 
        ];
        
    }

    /**
     * Calculates the linear value of an interpolation
     * 
     * Shamelessly stolen from https://www.ajdesigner.com/phpinterpolation/linear_interpolation_equation.php
     * @var $x1 - minAmount (inferior interval in table)
     * @var $y1 - minFee (fee for inferior interval)
     * @var $x2 - asking amount
     * @var $x3 - maxAmount (superior interval in table)
     * @var $x3 - maxFee (superior interval fee)
     * 
     * @return $y2 - fee from interpolated amount
     */
    public function lerp(float $x1, float $y1, float $x2, float $x3, float $y3): float
    {
        return ( ($x2 - $x1) * ($y3 - $y1) / ($x3 - $x1) ) + $y1;
    }

}