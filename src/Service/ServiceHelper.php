<?php
declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service;

use Lendable\Interview\Interpolation\Model\LoanApplication;

class ServiceHelper
{

    /**
     * Round up the number to a multiple of 5
     * Stolen from https://stackoverflow.com/questions/4133859/round-up-to-nearest-multiple-of-five-in-php
     * 
     * @var float the number we would like to round up to
     * @var int the multiple we would like to wound up by 
     * 
     * @return float 
     */
    public static function roundUpToAny(float $n, int $x=5): float
    {
        return (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
    }

}